

// function TotalSalary(workingDays,salaryType,absences,tax,salary){
// 	if (salaryType=="daily") {
// 	let dailySalary = salary*(workingDays-absences);
// 	let taxes = dailySalary - (dailySalary*(tax/100));
// 	return taxes
// 	}

// 	else if (salaryType=="monthly"){
// 		let monthlySalary = salary-((salary/workingDays)*absences);
// 		let taxes = monthlySalary-(monthlySalary*(tax/100));
// 		return taxes;
// 	}
// 	else {
// 		alert("Input Proper Data");
// 	}
// }

// alert("your salary is P"+TotalSalary(22,"monthly",0,0,220000));



function salaryCalc(workingDays, salaryType, absences, tax, salary){
	// This is for monthly computation
	// let dailySalary = salary/workingDays;
	let daily = dailySalary(workingDays, salary)

	// salary without tax
	let actualSalary = 0;

	if(salaryType==="Monthly"){
		// actualSalary = salary - (dailySalary*absences);
		actualSalary = perMonthSalary(workingDays, absences, salary);
	}else if(salaryType ==="Daily"){
		// actualSalary = salary * (workingDays-absences);
		actualSalary = perDaySalary(workingDays, absences, salary)
	}

	// For tax computation
	// let taxed = actualSalary * (tax/100);
	let taxed = taxComputation(actualSalary, tax)

	// final salary
	// let netSalary = actualSalary - taxed;
	let netSal = netSalary(actualSalary, taxed)

	return netSal;
}

console.log(salaryCalc(22, "Monthly", 2, 10, 25000));
console.log(salaryCalc(22, "Daily", 3, 5, 1500));
console.log(salaryCalc(22, "Blabla", 2, 5, 1000));

// Using multiple functions

function dailySalary(workingDays, salary){
	return salary/workingDays
}

function perMonthSalary(workingDays, absences, salary){
	let daily = dailySalary(workingDays, salary);
	return salary - (daily*absences);
}

function perDaySalary(workingDays, absences, salary){
	return salary * (workingDays-absences);
}

function taxComputation(grossSalary, tax){
	return grossSalary * (tax/100);
}

function netSalary(grossSalary, taxed){
	return grossSalary - taxed;
}

