
let allTasks =[];


// Create a function that shows all our task
// Create a function that adds a task;
function showTasks(){
	// console.log(allTasks);

allTasks.forEach(function(indivTask,index){
	console.log("Task #"+(index+1)+": "+indivTask);
})
return null;
}


function addTask(newTask){
	allTasks.push(newTask);
	return newTask;
}
function deleteTask(taskNo){
	// delete via splice
	// return the taskNo to its real index
	let realIndex=taskNo-1;
	let deletedTask = allTasks[realIndex];
	allTasks.splice(realIndex,1);
	return deletedTask;
}

function editTask(taskNo, editedTask){
	let realIndex=taskNo-1;

	allTasks.splice(realIndex,1,editedTask);
	return editedTask;
}